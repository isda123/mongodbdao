import org.scalatestplus.play._
import play.api.test._
import play.api.test.Helpers._
import play.api.libs.json._
import javax.inject._
import reactivemongo.api.MongoDriver
import scala.concurrent.ExecutionContext.Implicits.global
import models._
import reactivemongo.bson._
import org.joda.time.DateTime
import scala.concurrent.Future
import org.scalatest.concurrent._
import play.api.mvc._

class ApiSpec extends PlaySpec with OneServerPerSuite with ScalaFutures with IntegrationPatience {
//User
      val _id = BSONObjectID("5785f89924b3440b87655b4f")
      val address = List(Address("Jl. Apel","Bandung","Indonesia",12345))
      val payment = List(PaymentMethod(PaymentType.Visa,1234,"12312wwadasd4",new DateTime()))
      val user = User(Some(_id),"tobi","gobi","usernamegobi","passgobi","tobi@gmail.com",address,payment)
  "User" should {
     "wsCall create a new user" in {
      
      val futureResult = wsCall(Call("post", "/api/user")).post(Json.toJson(user))
      val body = futureResult.futureValue.json
      body.\("code").toOption must not be empty
    }
    "wsCall update user" in {
      
      val futureResult = wsCall(Call("patch", "/api/user")).patch(Json.toJson(user.copy(first_name="update_first_name")))
      val body = futureResult.futureValue.json
      body.\("code").toOption must not be empty
      body.\("code") mustBe JsDefined(Json.toJson(200))
    } 
    "wsCall get all users" in {
      val futureResult = wsCall(Call("get", "/api/user")).get
      val body = futureResult.futureValue.json
      body.\("code") mustBe JsDefined(Json.toJson(200))
      body.\("data").toOption must not be empty
      
    }
    "wsCall get by username/id" in {
      val futureResult = wsCall(Call("get", s"/api/user/usernamegobi")).get
      val body = futureResult.futureValue.json
      body.\("code").toOption must not be empty
      body.\("data").toOption must not be empty
    }
   
  }
//Category
     val parentID = Some(BSONObjectID("5785f8b024b3440b87655b50"))
     val catID = Some(BSONObjectID("5785f8c424b3440b87655b52"))
     val ancestors : List[Ancestors] = List(
         Ancestors(parentID,"Home","home"),
         Ancestors(Some(BSONObjectID.generate()),"Outdoors","outdoors")
         )
     val category = Category( catID,"cleaning-tools",Some(ancestors),parentID,
          "Cleaning Tools",Some("The Best Cleaning Tools!"))
                    
  "Category" should {
     "wsCall create a new category" in {
      
      val futureResult = wsCall(Call("post", "/api/category")).post(Json.toJson(category))
      val body = futureResult.futureValue.json
      body.\("code").toOption must not be empty
    }
    "wsCall update category" in {
      
      val futureResult = wsCall(Call("patch", "/api/category")).patch(Json.toJson(category.copy(name="Cleaning Tools Update")))
      val body = futureResult.futureValue.json
      body.\("code").toOption must not be empty
      body.\("code") mustBe JsDefined(Json.toJson(200))
    } 
    "wsCall get all categories" in {
      val futureResult = wsCall(Call("get", "/api/category")).get
      val body = futureResult.futureValue.json
      body.\("code") mustBe JsDefined(Json.toJson(200))
      body.\("data").toOption must not be empty
      
    }
    "wsCall get by slug/id" in {
      val futureResult = wsCall(Call("get", s"/api/category/cleaning-tools")).get
      val body = futureResult.futureValue.json
      body.\("code").toOption must not be empty
      body.\("data").toOption must not be empty
    }
   
  }      
//Product
     val productID = Some(BSONObjectID("5785f8d924b3440b87655b57"))
     val details = Details( 50,"kg","ASW123","panasonic",Some("black"))
     val pricing = Pricing(Some(6000),5000)
     val history = List(PriceHistory(Some(6100),5100,DateTime.now(),DateTime.now()),
                        PriceHistory(Some(6300),5300,DateTime.now().plusMonths(1),DateTime.now().plusMonths(1)))
     val product = Product(productID,"vacuum-cleaner-1234","1234","Vacuum Cleaner",Some("The Best Vacuum Cleaner !"),
         details,Some(100),Some(4.5),pricing,Some(history),List(catID.get),parentID.get,Some(List("tools","cleaning","home")))
                    
  "Product" should {
     "wsCall create a new product" in {
      
      val futureResult = wsCall(Call("post", "/api/product")).post(Json.toJson(product))
      val body = futureResult.futureValue.json
      body.\("code").toOption must not be empty
    }
    "wsCall update product" in {
      
      val futureResult = wsCall(Call("patch", "/api/product")).patch(Json.toJson(product.copy(name="Vacuum Cleaner Update")))
      val body = futureResult.futureValue.json
      body.\("code").toOption must not be empty
      body.\("code") mustBe JsDefined(Json.toJson(200))
    } 
    "wsCall get all categories" in {
      val futureResult = wsCall(Call("get", "/api/product")).get
      val body = futureResult.futureValue.json
      body.\("code") mustBe JsDefined(Json.toJson(200))
      body.\("data").toOption must not be empty
      
    }
    "wsCall get by slug/id" in {
      val futureResult = wsCall(Call("get", s"/api/product/vacuum-cleaner-1234")).get
      val body = futureResult.futureValue.json
      body.\("code").toOption must not be empty
      body.\("data").toOption must not be empty
    }
   
  }      


}
