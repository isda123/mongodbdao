name := """salestock"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala,DebianPlugin)

scalaVersion := "2.11.7"

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

scalacOptions += "-feature"
routesGenerator := InjectedRoutesGenerator
libraryDependencies ++= Seq(
  jdbc,
  cache,
  ws,
  "org.scalatestplus.play" %% "scalatestplus-play" % "1.5.1" % Test,
   "org.reactivemongo" %% "play2-reactivemongo" % "0.11.14"
)

maintainer in Linux := "Al Kindi Isda <alkindiisda@gmail.com>"

packageSummary in Linux := "Yoo distribution"