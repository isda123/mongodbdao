package models
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import reactivemongo.bson._
import org.joda.time.DateTime
import play.api.libs.json.{Reads, Writes, Json}
import reactivemongo.api.indexes.{Index, IndexType}
import reactivemongo.play.json.BSONFormats._
import play.modules.reactivemongo._
import models.helper.Util.BSONDateTimeHandler
import models.helper._
import reactivemongo.api.DefaultDB

case class Review( _id:BSONObjectID,
                   product_id:String,
                   date:DateTime,
                   title:String,
                   text:String,
                   rating:Double,
                   user_id:BSONObjectID,
                   username:String,
                   helpful_votes:Option[Int],
                   voter_ids:List[BSONObjectID]
                   )
               
object Review {
  implicit val itemFormat = Json.format[Review]
  implicit val itemBSONFormat = Macros.handler[Review]
}
object ReviewDao extends {
  override val autoIndexes = Seq(
    Index(Seq("_id" -> IndexType.Ascending), unique = true, background = true)
  )} with BsonDao[Review, BSONObjectID]("review") {}