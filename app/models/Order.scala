package models
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import reactivemongo.bson.{BSONDocument,BSONObjectID, BSONDocumentReader, Macros }
import org.joda.time.DateTime
import play.api.libs.json.{Reads, Writes, Json}
import reactivemongo.api.indexes.{Index, IndexType}
import reactivemongo.play.json.BSONFormats._
import play.modules.reactivemongo._
import models.helper.Util.BSONDateTimeHandler
import models.helper._
import reactivemongo.api.DefaultDB

case class LineItems( _id:BSONObjectID,
                      sku:String,
                      name:String,
                      quantity:Int,
                      pricing:Pricing
                    )
case class Address( street:String,
                    city:String,
                    state:String,
                    zip:Int
                  )         
case class Order( _id:BSONObjectID,
                  user_id:BSONObjectID,
                  state:String,
                  line_items:Option[List[LineItems]],
                  shipping_address:Address,
                  sub_total:Int
                )
object LineItems {
  implicit val itemFormat = Json.format[LineItems]
  implicit val itemBSONFormat = Macros.handler[LineItems]
}                 
object Address {
  implicit val itemFormat = Json.format[Address]
  implicit val itemBSONFormat = Macros.handler[Address]
} 
object Order {
  implicit val itemFormat = Json.format[Order]
  implicit val itemBSONFormat = Macros.handler[Order]
} 
object OrderDao extends {
  override val autoIndexes = Seq(
    Index(Seq("_id" -> IndexType.Ascending), unique = true, background = true)
  )} with BsonDao[Order, BSONObjectID]("order") {}