package models
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import reactivemongo.bson._
import org.joda.time.DateTime
import reactivemongo.api.indexes.{Index, IndexType}
import reactivemongo.api.DefaultDB
import play.api.libs.json.{Reads, Writes, Json}
import reactivemongo.play.json.BSONFormats._
import play.modules.reactivemongo._
import models.helper.Util.{BSONDateTimeHandler,EnumUtils}
import models.helper._


object PaymentType extends Enumeration {
  type PaymentType = Value
  val Visa = Value("Visa")
  val Master = Value("Master")

  implicit val enumReads: Reads[PaymentType] = EnumUtils.enumReads(PaymentType)
  implicit def enumWrites: Writes[PaymentType] = EnumUtils.enumWrites
  implicit object BSONEnumHandler extends BSONHandler[BSONInteger, PaymentType] {
    def read(doc: BSONInteger) = PaymentType.values.find(_.id == doc.value).get
    def write(value: PaymentType) = BSON.write(value.id)
  }

}
case class PaymentMethod( `type`:PaymentType.PaymentType,
                          last_four:Int,
                          crypted_number:String,
                          expiration_date:DateTime
                         )
case class User(_id: Option[BSONObjectID],
                first_name: String,
                last_name: String,
                username: String,
                password: String,
                email: String,
                addresses:List[Address],
                payment_methods:List[PaymentMethod]
                )

object PaymentMethod {
  implicit val detailsFormat = Json.format[PaymentMethod]
  implicit val detailsBSONFormat = Macros.handler[PaymentMethod]
}                
object User {
  implicit val userFormat = Json.format[User]
  implicit val userBSONFormat = Macros.handler[User]
}


object UserDao extends {
  override val autoIndexes = Seq(
    Index(Seq("username" -> IndexType.Ascending), unique = true, background = true)
  )} with BsonDao[User, BSONObjectID]("user") {}