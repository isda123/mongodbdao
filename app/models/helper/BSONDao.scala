package models.helper
import reactivemongo.bson._
import reactivemongo.api.collections.bson._
import play.api.libs.json.{Json}
import play.modules.reactivemongo._
import reactivemongo.core.commands._
import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.concurrent.duration._
import javax.inject._
import reactivemongo.api.{ DB, QueryOpts , Collection, CollectionProducer}
import reactivemongo.api.commands.{ GetLastError, WriteResult }
import scala.language.higherKinds
import reactivemongo.api.indexes.Index
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.Random
import scala.language.postfixOps
import play.api.libs.iteratee.{ Iteratee, Enumerator }
import reactivemongo.api.DefaultDB

abstract class BsonDao[Model, ID](collectionName: String)(implicit modelReader: BSONDocumentReader[Model],
  modelWriter: BSONDocumentWriter[Model],
  idWriter: BSONWriter[ID, _ <: BSONValue],
  idReader: BSONReader[_ <: BSONValue, ID],
  lifeCycle: LifeCycle[Model, ID] = new ReflexiveLifeCycle[Model, ID],
  ec: ExecutionContext) extends Dao[BSONCollection, BSONDocument, Model, ID, BSONDocumentWriter](collectionName) {
  
  def ensureIndexes()(implicit ec: ExecutionContext): Future[Traversable[Boolean]] = Future sequence {
    autoIndexes map { index =>
      collection.flatMap(_.indexesManager.ensure(index))
    
    }
  }.map { results =>
    lifeCycle.ensuredIndexes()
    results
  }
  
  def listIndexes()(implicit ec: ExecutionContext): Future[List[Index]] =
     collection.flatMap(_.indexesManager.list())

  def findOne(selector: BSONDocument = BSONDocument.empty)(implicit ec: ExecutionContext): Future[Option[Model]] =  collection.flatMap(_.find(selector).one[Model])

  def findById(id: ID)(implicit ec: ExecutionContext): Future[Option[Model]] =
    findOne(BSONDocument("_id"->id))

  def findByIds(ids: ID*)(implicit ec: ExecutionContext): Future[List[Model]] =
    findAll(BSONDocument("_id" ->BSONDocument("$in"->(ids))))

  def find(
    selector: BSONDocument = BSONDocument.empty,
    sort: BSONDocument = BSONDocument("_id" -> 1),
    page: Int,
    pageSize: Int)(implicit ec: ExecutionContext): Future[List[Model]] = {
    val from = (page - 1) * pageSize
     collection.flatMap(_
      .find(selector)
      .sort(sort)
      .options(QueryOpts(skipN = from, batchSizeN = pageSize))
      .cursor[Model]()
      .collect[List](pageSize))
  }

  def findAll(
    selector: BSONDocument = BSONDocument.empty,
    sort: BSONDocument = BSONDocument("_id" -> 1))(implicit ec: ExecutionContext): Future[List[Model]] =
     collection.flatMap(_.find(selector).sort(sort).cursor[Model]().collect[List]())


  def findRandom(selector: BSONDocument = BSONDocument.empty)(implicit ec: ExecutionContext): Future[Option[Model]] = for {
    count <- count(selector)
    index = if (count == 0) 0 else Random.nextInt(count)
    random <-  collection.flatMap(_.find(selector).options(QueryOpts(skipN = index, batchSizeN = 1)).one[Model])
  } yield random

  def insert(model: Model, writeConcern: GetLastError = defaultWriteConcern)(implicit ec: ExecutionContext): Future[WriteResult] = {
    val mappedModel = lifeCycle.prePersist(model)
    collection.flatMap(_.insert(mappedModel, writeConcern)) map { writeResult =>
      lifeCycle.postPersist(mappedModel)
      writeResult
    }
  }

  private val (maxBulkSize, maxBsonSize): (Int, Int) = (1000,1000)

  def bulkInsert(
    documents: TraversableOnce[Model],
    bulkSize: Int = maxBulkSize,
    bulkByteSize: Int = maxBsonSize)(implicit ec: ExecutionContext): Future[Int] = {
    val mappedDocuments = documents.map(lifeCycle.prePersist)
    val writer = implicitly[BSONDocumentWriter[Model]]

    collection.flatMap(_.bulkInsert(mappedDocuments.map(writer.write(_)).toStream,
      true, defaultWriteConcern, bulkSize, bulkByteSize)) map { result =>
        mappedDocuments.map(lifeCycle.postPersist)
        result.n
      }
  }

  def update[U: BSONDocumentWriter](
    selector: BSONDocument,
    update: U,
    writeConcern: GetLastError = defaultWriteConcern,
    upsert: Boolean = false,
    multi: Boolean = false)(implicit ec: ExecutionContext): Future[WriteResult] = collection.flatMap(_.update(selector, update, writeConcern, upsert, multi))

  def updateById[U: BSONDocumentWriter](
    id: ID,
    update: U,
    writeConcern: GetLastError = defaultWriteConcern)(implicit ec: ExecutionContext): Future[WriteResult] = 
      collection.flatMap(_.update(BSONDocument("_id" -> id), update, writeConcern))

  def save(model: Model, writeConcern: GetLastError = defaultWriteConcern)(implicit ec: ExecutionContext): Future[WriteResult] = {
    val writer = implicitly[BSONDocumentWriter[Model]]

    for {
      doc <- Future(writer write model)
      _id <- Future(doc.getAs[ID]("_id").get)
      res <- {
        val mappedModel = lifeCycle.prePersist(model)
        collection.flatMap(_.update(selector = BSONDocument("_id" -> _id), update = mappedModel,
          upsert = true, writeConcern = writeConcern)) map { result =>
            lifeCycle.postPersist(mappedModel)
            result
          }
      }
    } yield res
  }

  def count(selector: BSONDocument = BSONDocument.empty)(implicit ec: ExecutionContext): Future[Int] = collection.flatMap(_.count(Some(selector)))

  def drop()(implicit ec: ExecutionContext): Future[Unit] = collection.flatMap(_.drop(true).map(_=>()))

  def dropSync(timeout: Duration = 10 seconds)(implicit ec: ExecutionContext): Unit = Future(drop(), timeout)

  def removeById(id: ID, writeConcern: GetLastError = defaultWriteConcern)(implicit ec: ExecutionContext): Future[WriteResult] = {
    lifeCycle.preRemove(id)
    collection.flatMap(_.remove(BSONDocument("_id" -> id), writeConcern = defaultWriteConcern)) map { res =>
      lifeCycle.postRemove(id)
      res
    }
  }

  def remove(
    query: BSONDocument,
    writeConcern: GetLastError = defaultWriteConcern,
    firstMatchOnly: Boolean = false)(implicit ec: ExecutionContext): Future[WriteResult] = collection.flatMap(_.remove(query, writeConcern, firstMatchOnly))

  def removeAll(writeConcern: GetLastError = defaultWriteConcern)(implicit ec: ExecutionContext): Future[WriteResult] = {
    collection.flatMap(_.remove(query = BSONDocument.empty, writeConcern = writeConcern, firstMatchOnly = false))
  }

  def foreach(
    selector: BSONDocument = BSONDocument.empty,
    sort: BSONDocument = BSONDocument("_id" -> 1))(f: (Model) => Unit)(implicit ec: ExecutionContext): Future[Unit] = {
    collection.flatMap(_.find(selector).sort(sort).cursor[Model]()
      .enumerate()
      .apply(Iteratee.foreach(f)))
      .flatMap(i => i.run)
  }

  def fold[A](
    selector: BSONDocument = BSONDocument.empty,
    sort: BSONDocument = BSONDocument("_id" -> 1),
    state: A)(f: (A, Model) => A)(implicit ec: ExecutionContext): Future[A] = {
    collection.flatMap(_.find(selector).sort(sort).cursor[Model]()
      .enumerate()
      .apply(Iteratee.fold(state)(f)))
      .flatMap(i => i.run)
  }
  ensureIndexes()
}