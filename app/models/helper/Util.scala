package models.helper

import reactivemongo.bson.{BSONHandler, BSONDateTime, Macros}
import org.joda.time.format.ISODateTimeFormat
import org.joda.time.{DateTime, DateTimeZone}
import play.api.libs.json._
import play.api.data.Forms
import play.api.data.Mapping
import play.api.data.FormError
import play.api.data.format.Formatter
import scala.language.implicitConversions
import reactivemongo.bson.{BSON, BSONInteger, BSONHandler}
import reactivemongo.play.json.BSONFormats

object Util {
  DateTimeZone.setDefault(DateTimeZone.UTC)

  implicit object BSONDateTimeHandler extends BSONHandler[BSONDateTime, DateTime] {
    val fmt = ISODateTimeFormat.dateTime()
    def read(time: BSONDateTime) = new DateTime(time.value)
    def write(jdtime: DateTime) = BSONDateTime(jdtime.getMillis)
  }
  
  implicit object BSONJsValueHandler extends {
    
    def read(js: JsValue ) = BSONFormats.BSONDocumentFormat.reads(js).get
    def write(js: JsValue ) = BSONFormats.BSONDocumentFormat.writes(read(js))
  }
  
  object EnumUtils {
  def enumReads[E <: Enumeration](enum: E): Reads[E#Value] = new Reads[E#Value] {
    def reads(json: JsValue): JsResult[E#Value] = json match {
      case JsNumber(s) => {
        enum.values.find(_.id == s) match {
          case Some(value) => JsSuccess(value)
          case None =>
            JsError(s"Enumeration expected of type: '${enum.getClass}', but it does not appear to contain the value: '$s'")
        }
      }
      case _ => JsError("Integer value expected")
    }
  }
  
  def enum[E <: Enumeration](enum: E): Mapping[E#Value] = Forms.of(enumForm(enum))

  def enumForm[E <: Enumeration](enum: E): Formatter[E#Value] = new Formatter[E#Value] {
    def bind(key: String, data: Map[String, String]) = {
      play.api.data.format.Formats.intFormat.bind(key, data).right.flatMap { s =>
        enum.values.find(_.id == s) match {
          case Some(value) => Right(value)
          case None => Left(Seq(FormError(key, "Invalid enum value", Nil)))
        }
      }
    }
    def unbind(key: String, value: E#Value) = Map(key -> value.id.toString)
  }
 
  implicit def enumWrites[E <: Enumeration]: Writes[E#Value] = new Writes[E#Value] {
    def writes(v: E#Value): JsValue = JsNumber(v.id)
  }
 
  implicit def enumFormat[E <: Enumeration](enum: E): Format[E#Value] = {
    Format(EnumUtils.enumReads(enum), EnumUtils.enumWrites)
  }

}
  
}