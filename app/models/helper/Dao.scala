package models.helper

import reactivemongo.bson._
import reactivemongo.api.collections.bson._
import play.api.libs.json.{Json}
import play.modules.reactivemongo._
import reactivemongo.core.commands._
import scala.concurrent.{ExecutionContext, Future, Promise}
import scala.concurrent.duration._
import javax.inject._
import reactivemongo.api.{ DB, QueryOpts , Collection, CollectionProducer,DefaultDB}
import reactivemongo.api.commands.{ GetLastError, WriteResult }
import scala.language.higherKinds
import reactivemongo.api.indexes.Index
import scala.concurrent.ExecutionContext.Implicits.global
import play.api.Play.current
import reactivemongo.api.{MongoDriver,MongoConnection}
import reactivemongo.core.nodeset.Authenticate

trait LifeCycle[Model, ID] {
  def prePersist(model: Model): Model
  def postPersist(model: Model): Unit
  def preRemove(id: ID): Unit
  def postRemove(id: ID): Unit
  def ensuredIndexes(): Unit
}

class ReflexiveLifeCycle[Model, ID] extends LifeCycle[Model, ID] {
  def prePersist(model: Model): Model = model
  def postPersist(model: Model): Unit = {}
  def preRemove(id: ID): Unit = {}
  def postRemove(id: ID): Unit = {}
  def ensuredIndexes(): Unit = {}
}

abstract class Dao[C <: Collection: CollectionProducer, Structure, Model, ID, Writer[_]](collectionName: String) {
private lazy val driver = new MongoDriver()
private lazy val config = current.configuration
private lazy val server = config.getStringList("mongodb.servers").getOrElse(List())
private lazy val uri    = config.getString("mongodb.uri").getOrElse("")
private lazy val dbname = config.getString("mongodb.db").getOrElse("")
private lazy val connection = driver.connection(MongoConnection.parseURI(uri).get)

 
//val connection = driver.connection(servers, nbChannelsPerNode = 5, authentications = credentials))
def autoIndexes: Traversable[Index] = Seq.empty

  /**
   * Bulk inserts multiple models.
   *
   * @param models A [[scala.collection.TraversableOnce]] of models.
   * @param bulkSize
   * @param bulkByteSize
   * @return The number of successful insertions.
   */
  def bulkInsert(models: TraversableOnce[Model], bulkSize: Int, bulkByteSize: Int)(implicit ec: ExecutionContext): Future[Int]

  /** Reference to the collection this DAO operates on. */
  def collection: Future[C] =  connection.database(dbname).flatMap{db=>Future(db.collection(collectionName))}

  /**
   * Returns the number of documents in this collection matching the given selector.
   *
   * @param selector Selector document which may be empty.
   */
  def count(selector: Structure)(implicit ec: ExecutionContext): Future[Int]

  /**
   * Defines the default write concern for this Dao which defaults to `GetLastError()`.
   *
   * Related API functions should allow overriding this value.
   */
  def defaultWriteConcern: GetLastError = GetLastError.Default

  /** Drops this collection */
  def drop()(implicit ec: ExecutionContext): Future[Unit]

  /**
   * Drops this collection and awaits until it has been dropped or a timeout has occured.
   * @param timeout Maximum amount of time to await until this collection has been dropped.
   * @return true if the collection has been successfully dropped, otherwise false.
   */
  def dropSync(timeout: Duration)(implicit ec: ExecutionContext): Unit

  /** Ensures indexes defined by `autoIndexes`. */
  def ensureIndexes()(implicit ec: ExecutionContext): Future[Traversable[Boolean]]

  /**
   * Retrieves models by page matching the given selector.
   *
   * @param selector Selector document.
   * @param sort Sorting document.
   * @param page 1 based page number.
   * @param pageSize Maximum number of elements in each page.
   */
  def find(selector: Structure, sort: Structure, page: Int, pageSize: Int)(implicit ec: ExecutionContext): Future[List[Model]]

  /**
   * Retrieves all models matching the given selector.
   *
   * @param selector Selector document.
   * @param sort Sorting document.
   */
  def findAll(selector: Structure, sort: Structure)(implicit ec: ExecutionContext): Future[List[Model]]

  /**
   * Updates and returns a single model. It returns the old document by default.
   *
   * @param query The selection criteria for the update.
   * @param update Performs an update of the selected model.
   * @param sort Determines which model the operation updates if the query selects multiple models.
   *             findAndUpdate() updates the first model in the sort order specified by this argument.
   * @param fetchNewObject When true, returns the updated model rather than the original.
   * @param upsert When true, findAndUpdate() creates a new model if no model matches the query.
   */

  /** Retrieves the model with the given `id`. */
  def findById(id: ID)(implicit ec: ExecutionContext): Future[Option[Model]]

  /** Retrieves the models with the given `ids`. */
  def findByIds(ids: ID*)(implicit ec: ExecutionContext): Future[List[Model]]

  /** Retrieves at most one model matching the given selector. */
  def findOne(selector: Structure)(implicit ec: ExecutionContext): Future[Option[Model]]

  /**
   * Retrieves a random model matching the given selector.
   *
   * This API may require more than one query.
   */
  def findRandom(selector: Structure)(implicit ec: ExecutionContext): Future[Option[Model]]

  /**
   * Folds the documents matching the given selector by applying the function `f`.
   *
   * @param selector Selector document.
   * @param sort Sorting document.
   * @param state Initial state for the fold operation.
   * @param f Folding function.
   * @tparam A Type of fold result.
   */
  def fold[A](selector: Structure, sort: Structure, state: A)(f: (A, Model) => A)(implicit ec: ExecutionContext): Future[A]

  /**
   * Iterates over the documents matching the given selector and applies the function `f`.
   *
   * @param selector Selector document.
   * @param sort Sorting document.
   * @param f function to be applied.
   */
  def foreach(selector: Structure, sort: Structure)(f: (Model) => Unit)(implicit ec: ExecutionContext): Future[Unit]

  /** Inserts the given model. */
  def insert(model: Model, writeConcern: GetLastError)(implicit ec: ExecutionContext): Future[WriteResult]

  /**
   * Lists indexes that are currently ensured in this collection.
   *
   * This list may not be equal to `autoIndexes` in case of index creation failure.
   */
  def listIndexes()(implicit ec: ExecutionContext): Future[List[Index]]

  /**
   * Removes model(s) matching the given selector.
   *
   * In order to remove multiple documents `firstMatchOnly` has to be `false`.
   *
   * @param selector Selector document.
   * @param writeConcern Write concern defaults to `defaultWriteConcern`.
   * @param firstMatchOnly Remove only the first matching document.
   */
  def remove(
    selector: Structure,
    writeConcern: GetLastError,
    firstMatchOnly: Boolean)(implicit ec: ExecutionContext): Future[WriteResult]

  /** Removes all documents in this collection. */
  def removeAll(writeConcern: GetLastError)(implicit ec: ExecutionContext): Future[WriteResult]

  /** Removes the document with the given ID. */
  def removeById(id: ID, writeConcern: GetLastError)(implicit ec: ExecutionContext): Future[WriteResult]

  /**
   * Inserts the document, or updates it if it already exists in the collection.
   *
   * @param model The model to save.
   * @param writeConcern the [[reactivemongo.core.commands.GetLastError]] command message to send in order to control
   *                     how the document is inserted. Defaults to defaultWriteConcern.
   */
  def save(model: Model, writeConcern: GetLastError)(implicit ec: ExecutionContext): Future[WriteResult]

  /**
   * Updates the documents matching the given selector.
   *
   * @param selector Selector query.
   * @param update Update query.
   * @param writeConcern Write concern which defaults to defaultWriteConcern.
   * @param upsert Create the document if it does not exist.
   * @param multi Update multiple documents.
   * @tparam U Type of the update query.
   */
  def update[U: Writer](
    selector: Structure,
    update: U,
    writeConcern: GetLastError,
    upsert: Boolean,
    multi: Boolean)(implicit ec: ExecutionContext): Future[WriteResult]

  /**
   * Updates the document with the given `id`.
   *
   * @param id ID of the document that will be updated.
   * @param update Update query.
   * @param writeConcern Write concern which defaults to defaultWriteConcern.
   * @tparam U Type of the update query.
   */
  def updateById[U: Writer](id: ID, update: U, writeConcern: GetLastError)(implicit ec: ExecutionContext): Future[WriteResult]
}

