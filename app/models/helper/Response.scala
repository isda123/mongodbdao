package models.helper
import play.api.libs.json._
import reactivemongo.bson._
import play.modules.reactivemongo.json.BSONFormats._

case class Response(data:Option[JsValue]=None,message:JsValue=Json.toJson("success"),status:String="success",code:Int=200){
  def get = {
    Json.obj("data"->data,"message"->message,"status"->status,"code"->code)
  }
}
