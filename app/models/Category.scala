package models
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import reactivemongo.bson.{BSONDocument,BSONObjectID, BSONDocumentReader, Macros }
import org.joda.time.DateTime
import play.api.libs.json.{Reads, Writes, Json}
import reactivemongo.api.indexes.{Index, IndexType}
import reactivemongo.play.json.BSONFormats._
import play.modules.reactivemongo._
import models.helper.Util.BSONDateTimeHandler
import models.helper._
import reactivemongo.api.DefaultDB

case class Ancestors( _id:Option[BSONObjectID],name:String,slug:String)
                
case class Category( _id:Option[BSONObjectID],
                    slug:String,
                    ancestors:Option[List[Ancestors]],
                    parent_id:Option[BSONObjectID],
                    name:String,
                    description:Option[String]
                   )
object Ancestors {
  implicit val itemFormat = Json.format[Ancestors]
  implicit val itemBSONFormat = Macros.handler[Ancestors]
}                 
object Category {
  implicit val productFormat = Json.format[Category]
  implicit val productBSONFormat = Macros.handler[Category]
}
object CategoryDao extends {
  override val autoIndexes = Seq(
    Index(Seq("slug" -> IndexType.Ascending), unique = true, background = true)
  )} with BsonDao[Category, BSONObjectID]("category") {}