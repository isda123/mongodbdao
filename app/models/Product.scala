package models
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import reactivemongo.bson._
import org.joda.time.DateTime
import play.api.libs.json.{Reads, Writes, Json}
import reactivemongo.api.indexes.{Index, IndexType}
import reactivemongo.api.DefaultDB
import scala.concurrent.Future
import reactivemongo.play.json.BSONFormats._
import play.modules.reactivemongo._
import models.helper.Util.BSONDateTimeHandler
import models.helper._

case class Details( weight:Int,
                    weight_units:String,
                    model_num:String,
                    manufacturer:String,
                    color:Option[String]
                   )
case class Pricing(retail:Option[Long],sale:Long)
case class PriceHistory(retail:Option[Long],sale:Long,start:DateTime,end:DateTime)
case class Product( _id:Option[BSONObjectID],
                    slug:String,
                    sku:String,
                    name:String,
                    description:Option[String],
                    details: Details,
                    total_reviews:Option[Int],
                    average_review:Option[Double],
                    pricing:Pricing,
                    price_history:Option[List[PriceHistory]],
                    category_ids:List[BSONObjectID],
                    main_cat_id:BSONObjectID,
                    tags:Option[List[String]]
                   )
object Details {
  implicit val detailsFormat = Json.format[Details]
  implicit val detailsBSONFormat = Macros.handler[Details]
}
object Pricing {
  implicit val pricingFormat = Json.format[Pricing]
  implicit val pricingBSONFormat = Macros.handler[Pricing]
}
object PriceHistory {
  implicit val priceHistoryFormat = Json.format[PriceHistory]
  implicit val priceHistoryBSONFormat = Macros.handler[PriceHistory]
}
                   
object Product {
  implicit val productFormat = Json.format[Product]
  implicit val productBSONFormat = Macros.handler[Product]
}
object ProductDao extends {
  override val autoIndexes = Seq(
    Index(Seq("slug" -> IndexType.Ascending), unique = true, background = true)
  )} with BsonDao[Product, BSONObjectID]("product") {}