package controllers

import akka.actor.ActorSystem
import javax.inject._
import play.api._
import play.api.mvc._
import scala.concurrent.{ Future, Promise}
import scala.concurrent.duration._
import models._
import reactivemongo.bson._
import reactivemongo.api.collections.bson._
import play.api.libs.json._
import play.modules.reactivemongo._
import reactivemongo.core.commands._
import models.helper._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.util.{Success,Failure}
import models.helper.BsonDsl._

@Singleton
class ProductController @Inject() (actorSystem: ActorSystem)
extends Controller {
  
  def getAllProduct(sizeMin:Option[Int],sizeMax:Option[Int],
                    priceMin:Option[Long],priceMax:Option[Long],color:Option[String]) = Action.async {implicit request =>
  val query = if(sizeMin.isDefined) $and("details.weight" $gte sizeMin.get) else BSONDocument.empty
  val query1= if(sizeMax.isDefined) $and("details.weight" $lte sizeMax.get).add(query) else query
  val query2= if(priceMin.isDefined) $and("pricing.sale" $gte priceMin.get).add(query1) else query1
  val query3= if(priceMax.isDefined) $and("pricing.sale" $lte priceMax.get).add(query2) else query2
  val query4= if(color.isDefined) BSONDocument("details.color"->color.get).add(query3) else query3
  ProductDao.findAll(query4).flatMap{products=>
      Future.successful(Ok(Response(message=Json.toJson("successfully get All products"),data=Some(Json.toJson(products))).get))
    }
  }
  
  def createAndUpdateProduct = Action.async(parse.json) { implicit request =>
    request.body.validate[Product].map { product =>
       request.method match{
          case "POST" => 
             ProductDao.insert(product.copy(_id=Some(BSONObjectID.generate))).map { lastError =>
                 Ok(Response(message=Json.toJson("successfully create a new product")).get)
              }.recover {
                case _ => 
                Ok(Response(message=Json.toJson(s"product ${product.slug} is already exist"),code=500,status="error").get)
                }
          case "PATCH" => updateProduct(product)
          case _ => 
            Future.successful(Ok(Response(message=Json.toJson(s"Request method ${request.method} was not found"),code=500,status="error").get))
            
       }
    }.recoverTotal{
      e=>
      Future.successful(Ok(Response(message=JsError.toJson(e),code=500,status="error").get))
    }
  }
  
  def getProduct(productID:BSONObjectID)= {
     ProductDao.findById(productID).flatMap {
          case Some(product) =>
           Future.successful(
               Ok(Response(data=Some(Json.toJson(product)),message=Json.toJson("successfully get product info")).get)
               )
          case None =>
           Future.successful(  
               Ok(Response(message=Json.toJson("product not found"),code=400,status="error").get)
               )
        }.recover {case _ => Ok(Response(message=Json.toJson("database error"),code=400,status="error").get)}
  }
  
  def deleteProduct(productID:BSONObjectID)= {
     ProductDao.removeById(productID).flatMap { lastError =>
          Future.successful(Ok(Response(message=Json.toJson("product successfully delete")).get))    
        }.recover {case _ => Ok(Response(message=Json.toJson("database error"),code=400,status="error").get)}
  }
  def updateProduct(product:Product)= {
    val selector = BSONDocument("slug"->product.slug)
     ProductDao.findOne(selector).flatMap {
          case Some(currentProduct) =>
          ProductDao.update($id(currentProduct._id.getOrElse(BSONObjectID.generate())),
              product.copy(_id=currentProduct._id)).flatMap{lastError=>
           Future.successful(Ok(Response(data=Some(Json.toJson(product)),message=Json.toJson("successfully update product info")).get)) 
          }
           
          case None =>
           Future.successful(  
               Ok(Response(message=Json.toJson("product not found"),code=400,status="error").get)
               )
        }.recover {case _ => Ok(Response(message=Json.toJson("database error"),code=400,status="error").get)}
  }
  
  
   def product(id:String) = Action.async{ implicit request =>
    BSONObjectID.parse(id) match{
      case Success(productID)=>
        request.method match{
          case "GET" => getProduct(productID)
          case "DELETE" => deleteProduct(productID)
          case _ =>
            Future.successful(Ok(Response(message=Json.toJson(s"Request method ${request.method} was not found"),code=500,status="error").get))
           
        }       
      case Failure(e) =>
        val selector = BSONDocument("slug"->id)
        request.method match{
          case "GET" =>  ProductDao.findOne(selector).flatMap{
                          case Some(product)=>
                            Future.successful(Ok(Response(data=Some(Json.toJson(product)),message=Json.toJson("successfully get product info")).get))
                          case None =>
                            Future.successful(Ok(Response(message=Json.toJson("not found product"),code=500,status="error").get))
                        }
          case "DELETE" => ProductDao.remove(selector).flatMap{lastError=>
                            if(lastError.n==0)
                              Future.successful(Ok(Response(message=Json.toJson("not found product"),code=500,status="error").get))
                            else
                              Future.successful(Ok(Response(message=Json.toJson("product successfully delete")).get))    
                            }
          case _ =>
                    Future.successful(Ok(Response(message=Json.toJson(s"Request method ${request.method} was not found"),code=500,status="error").get))
           
        }   
       
       
    }
  }

}
