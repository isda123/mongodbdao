package controllers

import akka.actor.ActorSystem
import javax.inject._
import play.api._
import play.api.mvc._
import scala.concurrent.{ Future, Promise}
import scala.concurrent.duration._
import models._
import reactivemongo.bson._
import reactivemongo.api.collections.bson._
import play.api.libs.json._
import play.modules.reactivemongo._
import reactivemongo.core.commands._
import models.helper._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.util.{Success,Failure}
import models.helper.BsonDsl._

@Singleton
class AsyncController @Inject() (actorSystem: ActorSystem)
extends Controller {
  
  def getAllUser = Action.async {
  UserDao.findAll(BSONDocument.empty).flatMap{users=>
      Future.successful(Ok(Response(message=Json.toJson("successfully get All users"),data=Some(Json.toJson(users))).get))
    }
  }
  
  def createAndUpdateUser = Action.async(parse.json) { implicit request =>
    request.body.validate[User].map { user =>
       request.method match{
          case "POST" => 
             UserDao.insert(user.copy(_id=Some(BSONObjectID.generate))).map { lastError =>
                 Ok(Response(message=Json.toJson("successfully create a new user")).get)
              }.recover {
                case _ => 
                Ok(Response(message=Json.toJson(s"username ${user.username} is already exist"),code=500,status="error").get)
                }
          case "PATCH" => updateUser(user)
          case _ => 
            Future.successful(Ok(Response(message=Json.toJson(s"Request method ${request.method} was not found"),code=500,status="error").get))
            
       }
    }.recoverTotal{
      e=>
      Future.successful(Ok(Response(message=JsError.toJson(e),code=500,status="error").get))
    }
  }
  
  def getUser(userID:BSONObjectID)= {
     UserDao.findById(userID).flatMap {
          case Some(user) =>
           Future.successful(
               Ok(Response(data=Some(Json.toJson(user)),message=Json.toJson("successfully get user info")).get)
               )
          case None =>
           Future.successful(  
               Ok(Response(message=Json.toJson("user not found"),code=400,status="error").get)
               )
        }.recover {case _ => Ok(Response(message=Json.toJson("database error"),code=400,status="error").get)}
  }
  
  def deleteUser(userID:BSONObjectID)= {
     UserDao.removeById(userID).flatMap { lastError =>
          Future.successful(Ok(Response(message=Json.toJson("user successfully delete")).get))    
        }.recover {case _ => Ok(Response(message=Json.toJson("database error"),code=400,status="error").get)}
  }
  def updateUser(user:User)= {
    val selector = BSONDocument("username"->user.username)
     UserDao.findOne(selector).flatMap {
          case Some(currentUser) =>
          UserDao.update($id(currentUser._id.getOrElse(BSONObjectID.generate())),
              user.copy(_id=currentUser._id)).flatMap{lastError=>
           Future.successful(Ok(Response(data=Some(Json.toJson(user)),message=Json.toJson("successfully update user info")).get)) 
          }
           
          case None =>
           Future.successful(  
               Ok(Response(message=Json.toJson("user not found"),code=400,status="error").get)
               )
        }.recover {case _ => Ok(Response(message=Json.toJson("database error"),code=400,status="error").get)}
  }
  
  
   def user(id:String) = Action.async{ implicit request =>
    BSONObjectID.parse(id) match{
      case Success(userID)=>
        request.method match{
          case "GET" => getUser(userID)
          case "DELETE" => deleteUser(userID)
          case _ =>
            Future.successful(Ok(Response(message=Json.toJson(s"Request method ${request.method} was not found"),code=500,status="error").get))
           
        }       
      case Failure(e) =>
        val selector = BSONDocument("username"->id)
        request.method match{
          case "GET" =>  UserDao.findOne(selector).flatMap{
                          case Some(user)=>
                            Future.successful(Ok(Response(data=Some(Json.toJson(user)),message=Json.toJson("successfully get user info")).get))
                          case None =>
                            Future.successful(Ok(Response(message=Json.toJson("not found user"),code=500,status="error").get))
                        }
          case "DELETE" => UserDao.remove(selector).flatMap{lastError=>
                            if(lastError.n==0)
                              Future.successful(Ok(Response(message=Json.toJson("not found user"),code=500,status="error").get))
                            else
                              Future.successful(Ok(Response(message=Json.toJson("user successfully delete")).get))    
                            }
          case _ =>
                    Future.successful(Ok(Response(message=Json.toJson(s"Request method ${request.method} was not found"),code=500,status="error").get))
           
        }   
       
       
    }
  }

}
