package controllers

import akka.actor.ActorSystem
import javax.inject._
import play.api._
import play.api.mvc._
import scala.concurrent.{ Future, Promise}
import scala.concurrent.duration._
import models._
import reactivemongo.bson._
import reactivemongo.api.collections.bson._
import play.api.libs.json._
import play.modules.reactivemongo._
import reactivemongo.core.commands._
import models.helper._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.util.{Success,Failure}
import models.helper.BsonDsl._

@Singleton
class CategoryController @Inject() (actorSystem: ActorSystem)
extends Controller {
  
  def getAllCategory = Action.async {
  CategoryDao.findAll(BSONDocument.empty).flatMap{categories=>
      Future.successful(Ok(Response(message=Json.toJson("successfully get All categories"),data=Some(Json.toJson(categories))).get))
    }
  }
  
  def createAndUpdateCategory = Action.async(parse.json) { implicit request =>
    request.body.validate[Category].map { category =>
       request.method match{
          case "POST" => 
             CategoryDao.insert(category.copy(_id=Some(BSONObjectID.generate))).map { lastError =>
                 Ok(Response(message=Json.toJson("successfully create a new category")).get)
              }.recover {
                case _ => 
                Ok(Response(message=Json.toJson(s"category ${category.slug} is already exist"),code=500,status="error").get)
                }
          case "PATCH" => updateCategory(category)
          case _ => 
            Future.successful(Ok(Response(message=Json.toJson(s"Request method ${request.method} was not found"),code=500,status="error").get))
            
       }
    }.recoverTotal{
      e=>
      Future.successful(Ok(Response(message=JsError.toJson(e),code=500,status="error").get))
    }
  }
  
  def getCategory(categoryID:BSONObjectID)= {
     CategoryDao.findById(categoryID).flatMap {
          case Some(category) =>
           Future.successful(
               Ok(Response(data=Some(Json.toJson(category)),message=Json.toJson("successfully get category info")).get)
               )
          case None =>
           Future.successful(  
               Ok(Response(message=Json.toJson("category not found"),code=400,status="error").get)
               )
        }.recover {case _ => Ok(Response(message=Json.toJson("database error"),code=400,status="error").get)}
  }
  
  def deleteCategory(categoryID:BSONObjectID)= {
     CategoryDao.removeById(categoryID).flatMap { lastError =>
          Future.successful(Ok(Response(message=Json.toJson("category successfully delete")).get))    
        }.recover {case _ => Ok(Response(message=Json.toJson("database error"),code=400,status="error").get)}
  }
  def updateCategory(category:Category)= {
    val selector = BSONDocument("slug"->category.slug)
     CategoryDao.findOne(selector).flatMap {
          case Some(currentCategory) =>
          CategoryDao.update($id(currentCategory._id.getOrElse(BSONObjectID.generate())),
              category.copy(_id=currentCategory._id)).flatMap{lastError=>
           Future.successful(Ok(Response(data=Some(Json.toJson(category)),message=Json.toJson("successfully update category info")).get)) 
          }
           
          case None =>
           Future.successful(  
               Ok(Response(message=Json.toJson("category not found"),code=400,status="error").get)
               )
        }.recover {case _ => Ok(Response(message=Json.toJson("database error"),code=400,status="error").get)}
  }
  
  
   def category(id:String) = Action.async{ implicit request =>
    BSONObjectID.parse(id) match{
      case Success(categoryID)=>
        request.method match{
          case "GET" => getCategory(categoryID)
          case "DELETE" => deleteCategory(categoryID)
          case _ =>
            Future.successful(Ok(Response(message=Json.toJson(s"Request method ${request.method} was not found"),code=500,status="error").get))
           
        }       
      case Failure(e) =>
        val selector = BSONDocument("slug"->id)
        request.method match{
          case "GET" =>  CategoryDao.findOne(selector).flatMap{
                          case Some(category)=>
                            Future.successful(Ok(Response(data=Some(Json.toJson(category)),message=Json.toJson("successfully get category info")).get))
                          case None =>
                            Future.successful(Ok(Response(message=Json.toJson("not found category"),code=500,status="error").get))
                        }
          case "DELETE" => CategoryDao.remove(selector).flatMap{lastError=>
                            if(lastError.n==0)
                              Future.successful(Ok(Response(message=Json.toJson("not found category"),code=500,status="error").get))
                            else
                              Future.successful(Ok(Response(message=Json.toJson("category successfully delete")).get))    
                            }
          case _ =>
                    Future.successful(Ok(Response(message=Json.toJson(s"Request method ${request.method} was not found"),code=500,status="error").get))
           
        }   
       
       
    }
  }

}
